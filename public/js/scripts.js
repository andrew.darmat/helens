















$('.header-slider ul').slick({
    dots: false,
    infinite: true,
    speed: 500,
    fade: true,
    cssEase: 'linear'
  });






// $('.slider ul').slick({
//     dots: false,
//     infinite: true,
//     speed: 300,
//     slidesToShow: 5,
//     centerMode: true,
//     variableWidth: true,
//     prevArrow: $('.offers-prev '),
//     nextArrow: $('.offers-next'),
//   });

  $('.slider.slider--offers ul').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 1,
    centerMode: true,
    variableWidth: true,
    prevArrow: $('.slider__prev--offers '),
    nextArrow: $('.slider__next--offers')
  });

  $('.slider__prev--offers ').on("click", function(e){
      e.stopPropagation();
  });

  $('.slider__next--offers ').on("click", function(e){
    e.stopPropagation();
});

$('.slider.slider--blog > ul').slick({
  dots: false,
  infinite: true,
  speed: 300,
  slidesToShow: 1,
  slidesToScroll: 1,
  centerMode: true,
  variableWidth: true,
  prevArrow: $('.slider__prev--blog '),
  nextArrow: $('.slider__next--blog')
});

$('.slider__prev--blog ').on("click", function(e){
    e.stopPropagation();
});

$('.blog-slider__next--blog ').on("click", function(e){
  e.stopPropagation();
});


// var GeminiScrollbar = require('gemini-scrollbar')

var myScrollbar = new GeminiScrollbar({
    element: document.querySelector('.successfull__scrollbar'),
    // forceGemini: true
}).create();

const bodyTag = $("body");
const mobileMenu = $("#phoneMenu");


function showMenu(){
    mobileMenu.addClass("on");
    bodyTag.css("overflow", "hidden");
}

function hideMenu(){
    mobileMenu.removeClass("on");
    bodyTag.css("overflow", "auto");
}