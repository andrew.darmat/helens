const bodyTag = $("body");
const mobileMenu = $("#phoneMenu");


function showMenu(){
    mobileMenu.addClass("on");
    bodyTag.css("overflow", "hidden");
}

function hideMenu(){
    mobileMenu.removeClass("on");
    bodyTag.css("overflow", "auto");
}